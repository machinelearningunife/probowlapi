/*
 * Copyright 2017 MachineLearning@Unife - Department of Mathematics and Computer Science, 
            Department of Engineering - University of Ferrara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unife.ml.probowlapi.core;

import it.unife.ml.math.ApproxDouble;
import java.util.Set;
import javax.validation.constraints.NotNull;
import org.semanticweb.owlapi.model.OWLAxiom;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ProbabilisticExplanationReasonerResultImpl extends ProbabilisticReasonerResultImpl
        implements ProbabilisticExplanationReasonerResult {

    /**
     * Obtained explanations.
     */
    private Set<Set<OWLAxiom>> explanations;

    public ProbabilisticExplanationReasonerResultImpl(
            @NotNull
            OWLAxiom query, 
            @NotNull
            ApproxDouble queryProbability,
            @NotNull
            Set<Set<OWLAxiom>> explanations) {
        super(query, queryProbability);
        this.explanations = explanations;
    }


    @Override
    public Set<Set<OWLAxiom>> getQueryExplanations() {
        return explanations;
    }

}
