/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.monitor;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;
import it.unife.ml.probowlapi.renderer.LogManchesterSyntaxExplanationRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota
 * <giuseppe.cota@unife.it>
 */
public class LogRendererTimeExplanationProgressMonitor
        implements BundleRendererExplanationProgressMonitor, TimeMonitor {

    //Logger.getLogger(EDGE.class.getName());
    private LogManchesterSyntaxExplanationRenderer rend;
    private OWLAxiom axiom;
    private Set<Set<OWLAxiom>> setExplanations;
    private Logger logger;
    private boolean stop = false;
    private long execTime;
    private Thread t;

    public LogRendererTimeExplanationProgressMonitor(OWLAxiom axiom, Logger logger) {
        this.logger = logger;
        this.axiom = axiom;
        rend = new LogManchesterSyntaxExplanationRenderer();
        this.execTime = 0;

        setExplanations = new HashSet<>();
        try {
            rend.startRendering(logger);

        } catch (OWLException | IOException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }
    }

    public LogRendererTimeExplanationProgressMonitor(OWLAxiom axiom) {
        this(axiom, LoggerFactory.getLogger(LogRendererTimeExplanationProgressMonitor.class));
    }

    @Override
    public void foundExplanation(Set<OWLAxiom> axioms) {

        if (!setExplanations.contains(axioms)) {
            setExplanations.add(axioms);
            try {
                rend.render(axiom, Collections.singleton(axioms));
            } catch (IOException | OWLException e) {
                throw new RuntimeException("Error rendering explanation: " + e);
            }
        }
    }

    public void startMonitoring() {
        if (execTime > 0) {
            t = new Thread(new TimerChecker());
            t.start();
        }
    }

    @Override
    public void setParamAndStart(long time) {
        setExecTime(time);
        startMonitoring();
    }

    public void setExecTime(long time) {
        this.execTime = time;
    }

    public long getExecTime() {
        return execTime;
    }

    @Override
    public void stopMonitoring() {
        if (t != null && t.isAlive()) {
            t.stop();
        }
    }

    @Override
    public boolean isCancelled() {
//            if (execTimer == null)
//                return false;
//            else
//                //System.out.println("time " + execTimer.getElapsed() + " " + maxTime);
//                return (execTimer.getElapsed() > maxTime);
        return stop;
    }

    @Override
    public void foundAllExplanations() {
        try {
            rend.endRendering();
        } catch (OWLException | IOException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }
    }

    @Override
    public void foundNoExplanations() {
        try {
            rend.render(axiom, Collections.<Set<OWLAxiom>>emptySet());
            rend.endRendering();
        } catch (OWLException | IOException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }
    }

    @Override
    public void write(String s) {
        rend.write(s);
    }

    @Override
    public void writeln(String s) {
        rend.writeln(s);
    }

    private class TimerChecker implements Runnable {

        @Override
        public void run() {
            try {
                Thread.sleep(getExecTime());
                stop = true;

            } catch (InterruptedException ex) {
                throw new RuntimeException("Time monitors ended abnormally: " + ex);
            } catch (Exception ex) {
                throw new RuntimeException("Time monitors ended abnormally: " + ex);
            }

        }
    }

}
