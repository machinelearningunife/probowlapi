/*
 * Copyright 2017 MachineLearning@Unife - Department of Mathematics and Computer Science, 
            Department of Engineering - University of Ferrara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unife.ml.probowlapi.core;

import org.mindswap.pellet.utils.Timers;
import org.semanticweb.owlapi.model.OWLAxiom;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface ReasonerResult {

    /**
     * Return the executed query.
     * 
     * @return the query
     */
    public OWLAxiom getQuery();

    /**
     * Return the timings of the executed query.
     * 
     * @return the timers
     */
    public Timers getTimers();
    
    /**
     * Set the timings of the executed query.
     * 
     * @param timers
     */
    public void setTimers(Timers timers);

}
