/**
 *  This file is part of BUNDLE.
 * 
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 * 
 *  BUNDLE can be used both as module and as standalone.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.unife.ml.probowlapi.monitor;


/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class TimeMonitorImpl implements TimeMonitor {
    
    private boolean stop = false;
    private long execTime;
    private Thread t;
    
    public TimeMonitorImpl() {
        execTime = 0;
    }
    
    @Override
    public void startMonitoring() {
        if (execTime > 0) {
            t = new Thread(new TimerChecker());
            t.start();
        }
    }
    
    @Override
    public void setParamAndStart(long time) {
        setExecTime(time);
        startMonitoring();
    }
    
    public void setExecTime(long time) {
        this.execTime = time;
    }
    
    public long getExecTime() {
        return execTime;
    }
    
    @Override
    public void stopMonitoring() {
        if (t != null && t.isAlive())
            t.stop();
    }

    @Override
    public boolean isCancelled() {
//            if (execTimer == null)
//                return false;
//            else
//                //System.out.println("time " + execTimer.getElapsed() + " " + maxTime);
//                return (execTimer.getElapsed() > maxTime);
        return stop;
    }
    
    private class TimerChecker implements Runnable
    {
        public void run() {
            try {
                Thread.sleep(getExecTime());
                stop = true;
                
            } catch (InterruptedException ex) {
                //Logger.getLogger(PelletExplain.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                //Logger.getLogger(PelletExplain.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        }

}
}
