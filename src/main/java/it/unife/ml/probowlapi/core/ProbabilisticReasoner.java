/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.core;

import it.unife.ml.math.ApproxDouble;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import java.util.Map;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface ProbabilisticReasoner extends Reasoner {

    public ProbabilisticReasonerResult computeQuery(OWLAxiom query)
            throws OWLException, ObjectNotInitializedException;

    public void setpMap(Map<OWLAxiom, ApproxDouble> pMap);

    public Map<OWLAxiom, ApproxDouble> getpMap();
}
