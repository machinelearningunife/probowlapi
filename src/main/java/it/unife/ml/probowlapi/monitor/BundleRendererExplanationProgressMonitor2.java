/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.monitor;

import java.util.Set;
import org.semanticweb.owl.explanation.api.ConsoleExplanationProgressMonitor;
import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owl.explanation.api.ExplanationProgressMonitor;
import org.semanticweb.owlapi.model.OWLAxiom;



/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface BundleRendererExplanationProgressMonitor2<E> extends ExplanationProgressMonitor<E>, TimeMonitor {
    
    public void foundNoExplanations(OWLAxiom axiom);
    
    public Set<Explanation<E>> getFoundExplanations();
    
//    public void write(String s);
//    
//    public void writeln(String s);
}