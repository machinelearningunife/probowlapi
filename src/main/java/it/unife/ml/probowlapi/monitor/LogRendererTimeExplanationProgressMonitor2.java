/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.monitor;

import it.unife.ml.probowlapi.renderer.LogManchesterSyntaxExplanationRenderer;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owl.explanation.api.ExplanationGenerator;
import org.semanticweb.owl.explanation.api.ExplanationProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 * @param <E>
 */
public class LogRendererTimeExplanationProgressMonitor2<E extends OWLAxiom>
        extends TimeMonitorImpl
        implements BundleRendererExplanationProgressMonitor2<E> {

    //Logger.getLogger(EDGE.class.getName());
    private LogManchesterSyntaxExplanationRenderer rend;
    private Set<Explanation<E>> setExplanations;
    private final Logger logger;

    public LogRendererTimeExplanationProgressMonitor2() {
        this(LoggerFactory.getLogger(LogRendererTimeExplanationProgressMonitor2.class));
    }

    public LogRendererTimeExplanationProgressMonitor2(Logger logger) {
        this.logger = logger;
        rend = new LogManchesterSyntaxExplanationRenderer();
        setExplanations = new HashSet<>();
        try {
            rend.startRendering(logger);

        } catch (OWLException | IOException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }
    }

    @Override
    public void foundExplanation(ExplanationGenerator<E> generator, Explanation<E> explanation, Set<Explanation<E>> allFoundExplanations) {

        setExplanations.add(explanation);
        try {
            rend.render(Collections.singleton(explanation.getAxioms()));
        } catch (IOException | OWLException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }

    }

    @Override
    public void foundNoExplanations(OWLAxiom axiom) {
        try {
            rend.render(axiom, Collections.<Set<OWLAxiom>>emptySet());
//            rend.render(explanation.getEntailment(), Collections.<Set<OWLAxiom>>emptySet());
            rend.endRendering();
            rend = null;
        } catch (OWLException e) {
            System.err.println("Error rendering explanation: " + e);
        } catch (IOException e) {
            System.err.println("Error rendering explanation: " + e);
        }
    }

    @Override
    public boolean isCancelled() {
//        if (super.isCancelled()) {
//            try {
//                rend.endRendering();
//            } catch (OWLException | IOException e) {
//                System.err.println("Error rendering explanation: " + e);
//            }
//        }
        return super.isCancelled();
    }

    @Override
    public Set<Explanation<E>> getFoundExplanations() {
        return setExplanations;
    }

    public void addFoundExplanations(Explanation<E> setExplanations) {
        this.setExplanations.add(setExplanations);
    }

    public LogManchesterSyntaxExplanationRenderer getRend() {
        return rend;
    }
}
