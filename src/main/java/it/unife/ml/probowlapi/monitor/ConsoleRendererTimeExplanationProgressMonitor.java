/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.monitor;

import com.clarkparsia.owlapi.explanation.io.ExplanationRenderer;
import it.unife.ml.probowlapi.renderer.ManchesterSyntaxExplanationRendererExt;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owl.explanation.api.ExplanationGenerator;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 * @param <E>
 */
public class ConsoleRendererTimeExplanationProgressMonitor<E extends OWLAxiom>
        extends TimeMonitorImpl
        implements BundleRendererExplanationProgressMonitor2<E> {

//    private ExplanationRenderer rend = new ManchesterSyntaxExplanationRenderer();
    private ExplanationRenderer rend = new ManchesterSyntaxExplanationRendererExt();
    private OWLAxiom axiom;
    private Set<Explanation<E>> setExplanations;
    private PrintWriter pw;

    public ConsoleRendererTimeExplanationProgressMonitor(int timeout, PrintWriter pw) {
        this.pw = pw;
        setExplanations = new HashSet<>();
        try {
            rend.startRendering(pw);

        } catch (OWLException | IOException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }
    }

    public ConsoleRendererTimeExplanationProgressMonitor(int timeout) {
        this(timeout, new PrintWriter(System.out));
    }

    @Override
    public void foundExplanation(ExplanationGenerator<E> generator, Explanation<E> explanation, Set<Explanation<E>> allFoundExplanations) {
        setExplanations.add(explanation);
        try {
            rend.render(explanation.getEntailment(), Collections.singleton(explanation.getAxioms()));
            pw.flush();
        } catch (IOException | OWLException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }

    }

    @Override
    public void foundNoExplanations(OWLAxiom query) {
        try {
            rend.render(query, Collections.<Set<OWLAxiom>>emptySet());
//            rend.render(explanation.getEntailment(), Collections.<Set<OWLAxiom>>emptySet());
            rend.endRendering();
            rend = null;
        } catch (OWLException e) {
            System.err.println("Error rendering explanation: " + e);
        } catch (IOException e) {
            System.err.println("Error rendering explanation: " + e);
        }
    }

    @Override
    public boolean isCancelled() {
        pw.flush();
//        if (super.isCancelled()) {
//            try {
//                rend.endRendering();
//            } catch (OWLException | IOException e) {
//                System.err.println("Error rendering explanation: " + e);
//            }
//        }
        return super.isCancelled();
    }

    @Override
    public Set<Explanation<E>> getFoundExplanations() {
        return setExplanations;
    }

}
