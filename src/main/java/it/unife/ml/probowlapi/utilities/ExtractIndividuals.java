/**
 *  This file is part of BUNDLE.
 * 
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 * 
 *  BUNDLE can be used both as module and as standalone.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.unife.ml.probowlapi.utilities;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese <riccardo.zese@unife.it>
 */
public class ExtractIndividuals {

    /**
     * @param args the command line arguments
     * @throws OWLOntologyCreationException
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws OWLOntologyCreationException, IOException {
        if (args.length != 2) {
            System.out.println("Wrong number of arguments");
            System.exit(-1);
        }

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File(args[0]));
        PrintWriter outFile = new PrintWriter(new FileWriter(args[1]));
        Set<OWLNamedIndividual> setIndividuals = ontology.getIndividualsInSignature();
        System.out.println("Number of individuals: " + setIndividuals.size());
        int i = 1;
        for (OWLNamedIndividual ind : setIndividuals) {
            if (i == setIndividuals.size()) {
                outFile.write("\"" + ind.getIRI().toString().trim() + "\"");
            } else {
                outFile.write("\"" + ind.getIRI().toString().trim() + "\",\n");
            }
            i++;
        }
        outFile.flush();
        outFile.close();
    }

}
