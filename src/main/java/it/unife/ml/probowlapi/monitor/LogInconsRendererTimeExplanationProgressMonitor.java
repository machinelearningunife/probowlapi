/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.monitor;

import it.unife.ml.probowlapi.constants.UniFeIRI;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owl.explanation.api.ExplanationGenerator;
import org.semanticweb.owlapi.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.manchester.cs.owl.owlapi.OWLNamedIndividualImpl;

public class LogInconsRendererTimeExplanationProgressMonitor<E extends OWLAxiom>
        extends LogRendererTimeExplanationProgressMonitor2<E> {
    private OWLIndividual queryInd;

    public LogInconsRendererTimeExplanationProgressMonitor() {
        this(LoggerFactory.getLogger(LogInconsRendererTimeExplanationProgressMonitor.class));
    }

    public LogInconsRendererTimeExplanationProgressMonitor(Logger logger) {
        super(logger);
        this.queryInd = new OWLNamedIndividualImpl(IRI.create(UniFeIRI.QUERY_PLACEHOLDER));
    }

    @Override
    public void foundExplanation(ExplanationGenerator<E> generator, Explanation<E> explanation, Set<Explanation<E>> allFoundExplanations) {
        Set<OWLAxiom> axioms = new HashSet<>();
        for (OWLAxiom ax : explanation.getAxioms()) {
            if (!ax.getIndividualsInSignature().contains(queryInd)) {
                axioms.add(ax);
            }
        }
        Explanation<E> cleanedExpl = new Explanation<>(explanation.getEntailment(),axioms);
        this.addFoundExplanations(explanation);

        try {
            this.getRend().render(Collections.singleton(cleanedExpl.getAxioms()));
        } catch (OWLException | IOException var5) {
            throw new RuntimeException("Error rendering explanation: " + var5);
        }
    }

}
