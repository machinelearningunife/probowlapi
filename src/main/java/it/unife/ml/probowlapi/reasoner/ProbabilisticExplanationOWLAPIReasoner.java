/*
 * Copyright 2017 MachineLearning@Unife - Department of Mathematics and Computer Science, 
            Department of Engineering - University of Ferrara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unife.ml.probowlapi.reasoner;

import it.unife.ml.probowlapi.core.ExplanationReasonerResult;
import it.unife.ml.probowlapi.core.ProbabilisticExplanationReasoner;
import it.unife.ml.probowlapi.core.ProbabilisticReasonerResult;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 * @deprecated
 */
public abstract class ProbabilisticExplanationOWLAPIReasoner extends ProbabilisticOWLAPIReasoner
        implements ProbabilisticExplanationReasoner {

    protected int maxExplanations;

    @Override
    public abstract ProbabilisticReasonerResult computeQuery(OWLAxiom query)
            throws OWLException, ObjectNotInitializedException;

    @Override
    public abstract void dispose();

    @Override
    public abstract void init();

    @Override
    public abstract ExplanationReasonerResult explainAxiom(OWLAxiom query)
            throws OWLException, ObjectNotInitializedException;

    @Override
    public void setMaxExplanations(int maxExplanations) {
        this.maxExplanations = maxExplanations;
    }

    @Override
    public int getMaxExplanations() {
        return maxExplanations;
    }

}
