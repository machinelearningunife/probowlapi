/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.core;

import org.semanticweb.owlapi.model.OWLAxiom;
import it.unife.ml.math.ApproxDouble;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ProbabilisticAxiom {
    
    private final OWLAxiom axiom;
    
    private ApproxDouble probability;
    
    public ProbabilisticAxiom(OWLAxiom axiom, ApproxDouble probability) {
        this.axiom = axiom;
        this.probability = probability;
    }

    /**
     * @return the axiom
     */
    public OWLAxiom getAxiom() {
        return axiom;
    }

    /**
     * @return the probability
     */
    public ApproxDouble getProbability() {
        return probability;
    }

    /**
     * @param probability the probability to set
     */
    public void setProbability(ApproxDouble probability) {
        this.probability = probability;
    }
    
}
