/**
 *  This file is part of BUNDLE.
 * 
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 * 
 *  BUNDLE can be used both as module and as standalone.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.unife.ml.probowlapi.monitor;

import com.clarkparsia.owlapi.explanation.io.ExplanationRenderer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;
import it.unife.ml.probowlapi.renderer.ManchesterSyntaxExplanationRendererExt;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ScreenRendererTimeExplanationProgressMonitor
        extends TimeMonitorImpl
        implements BundleRendererExplanationProgressMonitor {

//    private ExplanationRenderer rend = new ManchesterSyntaxExplanationRenderer();
    private ExplanationRenderer rend = new ManchesterSyntaxExplanationRendererExt();
    private OWLAxiom axiom;
    private Set<Set<OWLAxiom>> setExplanations;
    private PrintWriter pw;

    public ScreenRendererTimeExplanationProgressMonitor(OWLAxiom axiom) {
        this.axiom = axiom;
        this.pw = new PrintWriter(System.out);

        setExplanations = new HashSet<>();
        try {
            rend.startRendering(pw);

        } catch (OWLException | IOException e) {
            throw new RuntimeException("Error rendering explanation: " + e);
        }
    }

//    public String RendererExplanationProgressMonitorString(OWLAxiom axiom) {
//        this.axiom = axiom;
//        StringWriter stringWriter = new StringWriter();
//        this.pw = new PrintWriter(stringWriter);
//
//        setExplanations = new HashSet<>();
//        try {
//            rend.startRendering(pw);
//            return stringWriter.toString();
//
//        } catch (OWLException | IOException e) {
//            throw new RuntimeException("Error rendering explanation: " + e);
//        }
//    }
    @Override
    public void foundExplanation(Set<OWLAxiom> axioms) {

        if (!setExplanations.contains(axioms)) {
            setExplanations.add(axioms);
            pw.flush();
            try {
                rend.render(axiom, Collections.singleton(axioms));
            } catch (IOException | OWLException e) {
                throw new RuntimeException("Error rendering explanation: " + e);
            }
        }
    }

    @Override
    public void foundAllExplanations() {
        try {
            rend.endRendering();
        } catch (OWLException | IOException e) {
            System.err.println("Error rendering explanation: " + e);
        }
    }

    @Override
    public void foundNoExplanations() {
        try {
            rend.render(axiom, Collections.<Set<OWLAxiom>>emptySet());
            rend.endRendering();
            rend = null;
        } catch (OWLException e) {
            System.err.println("Error rendering explanation: " + e);
        } catch (IOException e) {
            System.err.println("Error rendering explanation: " + e);
        }
    }

    @Override
    public void write(String s) {
        pw.print(s);
    }

    @Override
    public void writeln(String s) {
        pw.println(s);
    }

}
