/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.monitor;

import it.unife.ml.probowlapi.constants.UniFeIRI;
import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owl.explanation.api.ExplanationGenerator;
import org.semanticweb.owlapi.model.*;
import uk.ac.manchester.cs.owl.owlapi.OWLNamedIndividualImpl;

import java.util.HashSet;
import java.util.Set;

public class NullInconsRendererTimeExplanationProgressMonitor<E extends OWLAxiom>
        extends NullRendererTimeExplanationProgressMonitor<E> {
    private Set<Explanation<E>> setExplanations;
    private OWLIndividual queryInd;

    public NullInconsRendererTimeExplanationProgressMonitor() {
        this.queryInd = new OWLNamedIndividualImpl(IRI.create(UniFeIRI.QUERY_PLACEHOLDER));
        this.setExplanations = new HashSet();

    }

    public void foundExplanation(ExplanationGenerator<E> generator, Explanation<E> explanation, Set<Explanation<E>> allFoundExplanations) {
        Set<OWLAxiom> axioms = new HashSet<>();
        Boolean inconsExpl = true;
        for (OWLAxiom ax : explanation.getAxioms()) {
            if (!ax.getIndividualsInSignature().contains(queryInd)) {
                axioms.add(ax);
            } else {
                inconsExpl = false;
            }
        }
        Explanation<E> cleanedExpl = new Explanation<>(explanation.getEntailment(), axioms);
        this.setExplanations.add(cleanedExpl);

    }

    public void foundNoExplanations(OWLAxiom axiom) {
    }

    public Set<Explanation<E>> getFoundExplanations() {
        return this.setExplanations;
    }
}