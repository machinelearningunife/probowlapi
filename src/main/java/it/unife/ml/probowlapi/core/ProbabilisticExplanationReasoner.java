/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.core;

import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;

/**
 * Interface for Probabilistic reasoner that can obtain the probability and the
 * explanations of a query.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface ProbabilisticExplanationReasoner
        extends ExplanationReasoner, ProbabilisticReasoner {

    public ProbabilisticExplanationReasonerResult computeExplainQuery(OWLAxiom query)
            throws OWLException, ObjectNotInitializedException;
}
