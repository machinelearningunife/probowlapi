/*
 * Copyright 2017 MachineLearning@Unife - Department of Mathematics and Computer Science, 
            Department of Engineering - University of Ferrara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unife.ml.probowlapi.reasoner;

import it.unife.ml.math.ApproxDouble;
import it.unife.ml.probowlapi.core.ProbabilisticReasoner;
import it.unife.ml.probowlapi.core.ProbabilisticReasonerResult;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import java.util.Map;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

/**
 * Abstract class for a probabilistic reasoner. The probabilistic reasoner
 * contains a reasoner that implements the methods in the OWLAPI interface.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 * @deprecated
 */
public abstract class ProbabilisticOWLAPIReasoner implements ProbabilisticReasoner {

    protected OWLReasoner reasoner;

    protected OWLReasonerFactory reasonerFactory;

    /**
     * Reasoning time in milliseconds.
     */
    protected long reasoningTimeout;

    protected OWLOntology rootOntology;

    protected boolean initialized = false;
    
    protected Map<OWLAxiom, ApproxDouble> pMap;

    @Override
    public abstract ProbabilisticReasonerResult computeQuery(OWLAxiom query)
            throws OWLException, ObjectNotInitializedException;

    @Override
    public void setRootOntology(OWLOntology rootOntology) {
        this.rootOntology = rootOntology;
    }

    @Override
    public OWLOntology getRootOntology() {
        return rootOntology;
    }

    @Override
    public void setReasoningTimeout(long reasoningTimeout) {
        this.reasoningTimeout = reasoningTimeout;
    }

//    @Override
//    public void setReasoningTimeout(String reasoningTimeoutString) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    @Override
    public abstract void dispose();

    @Override
    public abstract void init() throws ExceptionInInitializerError;
//    {
//        reasoner = reasonerFactory.createReasoner(rootOntology);
//        this.isInitialized = true;
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    @Override
    public boolean isInitialized() {
        return initialized;
    }

    /**
     * @return the reasonerFactory
     */
    public OWLReasonerFactory getReasonerFactory() {
        return reasonerFactory;
    }

    /**
     * @param reasonerFactory the reasonerFactory to set
     */
    public void setReasonerFactory(OWLReasonerFactory reasonerFactory) {
        this.reasonerFactory = reasonerFactory;
    }

    /**
     * Return the inner OWLAPI reasoner.
     *
     * @return the reasoner
     */
    public OWLReasoner getReasoner() {
        return reasoner;
    }

    /**
     * @return the pMap
     */
    @Override
    public Map<OWLAxiom, ApproxDouble> getpMap() {
        return pMap;
    }

    /**
     * @param pMap the pMap to set
     */
    @Override
    public void setpMap(Map<OWLAxiom, ApproxDouble> pMap) {
        this.pMap = pMap;
    }
}
