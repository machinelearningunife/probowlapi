/* 
 * This file is part of ProbOWLAPI.
 * 
 * Copyright 2017 University of Ferrara.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unife.ml.probowlapi.constants;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese <riccardo.zese@unife.it>
 */
public final class UniFeIRI {

    public static final String DISPONTE_OLD = "https://sites.google.com/a/unife.it/ml/disponte";

    public static final String DISPONTE_HTTP = "http://ai.unife.it/disponte";

    public static final String DISPONTE = "https://ai.unife.it/disponte";

    public static final String PROBABILITY = "https://ai.unife.it/disponte#probability";

    public static final String ORIGIN_DOMAIN = "https://ai.unife.it/disponte#origin-domain";

    public static final String DISPONTE_ML_HTTP = "http://ml.unife.it/disponte";

    public static final String DISPONTE_ML = "https://ml.unife.it/disponte";

    public static final String PROBABILITY_ML = "https://ml.unife.it/disponte#probability";

    public static final String ORIGIN_DOMAIN_ML = "https://ml.unife.it/disponte#origin-domain";

    public static final String QUERY_PLACEHOLDER = "https://ai.unife.it/bundle#queryPlaceholder";
}