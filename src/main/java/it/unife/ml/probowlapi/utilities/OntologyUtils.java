package it.unife.ml.probowlapi.utilities;

import it.unife.ml.probowlapi.constants.UniFeIRI;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;

import java.util.Set;

public class OntologyUtils {

    public static void printInfoOntology(OWLOntology ontology) {
        int numIndividuals = ontology.getIndividualsInSignature(true).size();
        System.out.println("Number of individuals: " + numIndividuals);
        // migliorare ques'ultimo comando per poter contare anche gli assiomi importati
        int numAxioms = ontology.getLogicalAxiomCount();
        System.out.println("Number of logical axioms: " + numAxioms);
        numAxioms = ontology.getAxiomCount();
        System.out.println("Number of axioms: " + numAxioms);
    }

    public static Boolean isProbabilisticAxiom(OWLAxiom ax) {
        Set<OWLAnnotationProperty> anns = ax.getAnnotationPropertiesInSignature();
        for(OWLAnnotationProperty ann : anns) {
            IRI iriAnn = ann.getIRI();
            if (iriAnn.equals(IRI.create(UniFeIRI.DISPONTE + "#probability")) ||
                    iriAnn.equals(IRI.create(UniFeIRI.DISPONTE_HTTP + "#probability")) ||
                    iriAnn.equals(IRI.create(UniFeIRI.DISPONTE_OLD + "#probability")) ||
                    iriAnn.equals(IRI.create(UniFeIRI.DISPONTE_ML_HTTP + "#probability"))||
                    iriAnn.equals(IRI.create(UniFeIRI.DISPONTE_ML + "#probability"))) {
                return true;
            }
        }
        return false;
    }

    public static Boolean isProbabilisticOntology(OWLOntology ontology) {
        Set<OWLAxiom> axioms = ontology.getAxioms();
        for (OWLAxiom axiom : axioms) {
            if (isProbabilisticAxiom(axiom)) {
                return true;
            }
        }
        return false;
    }
}
