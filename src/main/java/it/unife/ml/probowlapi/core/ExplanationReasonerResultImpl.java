/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.core;

import java.util.Collections;
import java.util.Set;
import org.mindswap.pellet.utils.Timers;
import org.semanticweb.owlapi.model.OWLAxiom;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ExplanationReasonerResultImpl extends ReasonerResultImpl
        implements ExplanationReasonerResult {

    /**
     * Obtained explanations.
     */
    private Set<Set<OWLAxiom>> explanations;


    public ExplanationReasonerResultImpl(OWLAxiom query, Set<Set<OWLAxiom>> explanations) {
        super(query);
        this.explanations = explanations;
    }

//    public ExplanationResult(OWLAxiom query) {
//        this.query = query;
//        this.explanations = Collections.emptySet();
//        this.probabilisticUsedAxioms = Collections.emptySet();
//    }
    /**
     * @return the explanations
     */
    public Set<Set<OWLAxiom>> getQueryExplanations() {
        return explanations;
    }

}
