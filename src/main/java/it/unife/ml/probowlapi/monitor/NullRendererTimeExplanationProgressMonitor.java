/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.monitor;

import java.util.HashSet;
import java.util.Set;
import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owl.explanation.api.ExplanationGenerator;
import org.semanticweb.owlapi.model.OWLAxiom;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 * @param <E>
 */
public class NullRendererTimeExplanationProgressMonitor<E extends OWLAxiom>
        extends TimeMonitorImpl
        implements BundleRendererExplanationProgressMonitor2<E> {
    
    private final Set<Explanation<E>> setExplanations = new HashSet<>(); 

    @Override
    public void foundNoExplanations(OWLAxiom query) {
    }

    @Override
    public void foundExplanation(ExplanationGenerator<E> generator, Explanation<E> explanation, Set<Explanation<E>> allFoundExplanations) {
        setExplanations.add(explanation);
    }

    @Override
    public Set<Explanation<E>> getFoundExplanations() {
        return setExplanations;
    }

}
