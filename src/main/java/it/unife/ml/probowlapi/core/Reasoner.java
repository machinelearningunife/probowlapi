/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.probowlapi.core;

import org.semanticweb.owlapi.model.OWLOntology;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface Reasoner extends Initializable {

    /**
     * Set the "root" ontology for the reasoner.
     *
     * @param rootOntology
     */
    public void setRootOntology(OWLOntology rootOntology);

    /**
     * Get the "root" ontology loaded into the reasoner.
     *
     * @return
     */
    public OWLOntology getRootOntology();

    /**
     * @param reasoningTimeout the maximum time for execution to set (in
     * milliseconds)
     */
    public void setReasoningTimeout(long reasoningTimeout);

    /**
     * Returns the reasoning timeout in milliseconds
     *
     * @return the reasoningTimeout
     */
    public long getReasoningTimeout();

    /**
     * @param reasoningTimeoutString input string in the format -h-m-s-ms, where
     * '-' are the placeholder for the integer values of hours (h), minutes (m),
     * seconds (s) and milliseconds (ms). It is not mandatory that the string
     * contains all the four values, but the order must be followed.
     */
    //    public void setReasoningTimeout(String reasoningTimeoutString);
    /**
     * Free the memory. After the dispose reasoner could not be used without
     * initialization {@link #init}.
     */
    public void dispose();
}
